const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = function(env, options) {
  return {
    entry: [
      './js/index.js',
    ],
    output: {
      path: path.resolve(__dirname, '../assets'),
      filename: './js/bundle.js'
    },
    devtool: "source-map",
    module: {
      rules: [{
        test: /\.vue$/,
        loader: 'vue-loader'
      }, {
        test: /\.js$/,
        include: path.resolve(__dirname, 'js'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: [["@babel/preset-env", {
              targets: {
                browsers: ['last 2 version', 'not dead']
                // "chrome": "70",
              },
              useBuiltIns: 'usage'
            }]]
          }
        }
      }, {
        test: /\.less$|\.css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: MiniCssExtractPlugin.loader
        }, {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            url: false
          }
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: [
              autoprefixer({
                browsers: ['last 2 version', 'not dead']
              }),
              options.mode === 'production' ? cssnano({
                preset: 'default',
              }) : function(){}
            ],
            sourceMap: true
          }
        }, {
          loader: 'less-loader',
          options: {
            paths: [
              path.resolve(__dirname, 'css')
            ],
            sourceMap: true
          }
        }]
      }]
    },
    resolve: {
      alias: {
        'vue$': options.mode === 'production' ? 'vue/dist/vue.min.js' : 'vue/dist/vue.js' // 'vue'  // for runtime-only
      }
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: './css/bundle.css'
      }),
      new VueLoaderPlugin()
    ]
  };
};
