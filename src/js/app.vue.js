import Vue from 'vue';
import $ from 'jquery'; // для jsonp

new Vue({
  el: '#app',
  data: {
    query: '',
    images: [],
    favorites: [],
  },
  watch: {
    query: async function() {
      if (!this.query) {
        this.images = [];
        return;
      }

      try {
        var images = await this.search(this.query);
        if (images.response && images.response.items) {
          images = images.response.items.splice(0, 9);
          images = images.reduce(function(arr, item) {          
            var image = (item.sizes || []).find(function(size) {
              return size.type === 'r';
            });

            if (image) return arr.concat(image.url);
            else return arr;
          }, []);
        } else {
          imagees = [];
        }

        this.images = images;        
      } catch(error) {
        console.log(error, "error");
        this.images = []; 
      }
    }
  },
  methods: {
    search: function(query) { // через ключ приложения
      // var url = 'https://api.vk.com/method/photos.search?q=hello&v=5.92&access_token=bcf31b86bcf31b86bcf31b86babc9b2873bbcf3bcf31b86e0b5ef87f409d2dfcff0fc63';
      // var jsonpRequest = function(url) {
      //   return new Promise(function(resolve, reject) {
      //     window.callbackFunc = function(data) {
      //       console.log(err, "err");
      //       script.remove();
      //       resolve(data);
      //     }
      //     var script = document.createElement('script');
      //     script.src = url + '&callback=callbackFunc';
      //     document.getElementsByTagName('head')[0].appendChild(script);
      //   });
      // }

      return new Promise(function (resolve, reject) {
        $.ajax({
          url: 'https://api.vk.com/method/photos.search',
          dataType: "jsonp",
          data: {
            q: query,
            count: 20,
            v: '5.92',
            access_token: 'bcf31b86bcf31b86bcf31b86babc9b2873bbcf3bcf31b86e0b5ef87f409d2dfcff0fc63'
          }
        })
        .done(function (data) {
          resolve(data);
        })
        .fail(function (error) {
          reject(error);
        });
      });
    },
    addToFavorite: function(image) {
      this.favorites.push(image);
    },
    removeFromFavorite: function(index) {
      this.favorites.splice(index, 1);
    }
  }
});